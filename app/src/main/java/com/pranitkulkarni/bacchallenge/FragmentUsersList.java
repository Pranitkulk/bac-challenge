package com.pranitkulkarni.bacchallenge;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentUsersList extends Fragment {

    private List<UserModel> usersList = new ArrayList<>();
    RecyclerView recyclerView;

    public FragmentUsersList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_users_list, container, false);

         recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        usersList = new DatabaseManager(getActivity()).getUsers();

        if (usersList.size() > 0)
            recyclerView.setAdapter(new AdapterUsers(getActivity(),usersList));

        return view;
    }

}
