package com.pranitkulkarni.bacchallenge.Database;

import android.provider.BaseColumns;

/**
 * Created by pranitkulkarni on 8/1/17.
 */

public class DatabaseInfo {

    public DatabaseInfo(){}

    public static class Users implements BaseColumns{

        public static final String TABLE_NAME = "Users";

        public static final String ID = "id";

        public static final String IS_SELF = "is_self";
        public static final String NAME = "name";
        public static final String USER_NAME = "username";
        public static final String EMAIL = "email";
        public static final String PHONE  = "phone";
        public static final String WEBSITE  = "website";

        public static final String STREET  = "street";
        public static final String SUITE  = "suite";
        public static final String ZIPCODE  = "zipcode";
        public static final String CITY  = "city";

        public static final String LATITUDE  = "latitude";
        public static final String LONGITUDE  = "longitude";

        public static final String COMPANY_NAME  = "company_name";
        public static final String CATCH_PHRASE  = "catch_phrase";
        public static final String BS  = "bs";

    }

    public static class Friends implements BaseColumns{

        public static final String TABLE_NAME = "Friends";

        public static final String ID = "id";
        public static final String USER_ID = "user_id";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String SAVED_AT = "saved_at";

    }
}
