package com.pranitkulkarni.bacchallenge.Database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pranitkulkarni.bacchallenge.FriendModel;
import com.pranitkulkarni.bacchallenge.R;
import com.pranitkulkarni.bacchallenge.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pranitkulkarni on 8/1/17.
 */

public class DatabaseManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "challenge_users.db";

    private Context context;

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
        this.context = context;
    }

    private static final String CREATE_USERS_TABLE = "CREATE TABLE "+ DatabaseInfo.Users.TABLE_NAME + "(" +
            DatabaseInfo.Users.ID + " INTEGER PRIMARY KEY, " +
            DatabaseInfo.Users.NAME + " TEXT, "+
            DatabaseInfo.Users.USER_NAME + " TEXT, "+
            DatabaseInfo.Users.EMAIL + " TEXT, "+
            DatabaseInfo.Users.WEBSITE + " TEXT, "+
            DatabaseInfo.Users.PHONE + " TEXT, "+

            DatabaseInfo.Users.STREET + " TEXT, "+
            DatabaseInfo.Users.SUITE + " TEXT, "+
            DatabaseInfo.Users.ZIPCODE + " TEXT, "+
            DatabaseInfo.Users.CITY + " TEXT, "+

            DatabaseInfo.Users.LATITUDE + " TEXT, "+
            DatabaseInfo.Users.LONGITUDE + " TEXT, "+

            DatabaseInfo.Users.COMPANY_NAME + " TEXT, "+
            DatabaseInfo.Users.CATCH_PHRASE + " TEXT, "+

            DatabaseInfo.Users.IS_SELF + " INT, "+

            DatabaseInfo.Users.BS + " TEXT )";


    private static final String CREATE_FRIENDS_TABLE = "CREATE TABLE "+ DatabaseInfo.Friends.TABLE_NAME + "(" +
            DatabaseInfo.Friends.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            DatabaseInfo.Friends.USER_ID + " INTEGER, "+
            DatabaseInfo.Friends.LATITUDE + " TEXT," +
            DatabaseInfo.Friends.LONGITUDE + " TEXT," +
            DatabaseInfo.Friends.SAVED_AT + " TEXT ,"+
            "FOREIGN KEY ("+ DatabaseInfo.Friends.USER_ID + ") REFERENCES "+ DatabaseInfo.Users.TABLE_NAME + "(" + DatabaseInfo.Users.ID+"))";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_FRIENDS_TABLE);
        Log.d("Friends table created",CREATE_FRIENDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public Boolean saveUsers(List<UserModel> users){

        try{

            SQLiteDatabase database = this.getWritableDatabase();

            for (int i=0; i < users.size(); i++){

                final UserModel user = users.get(i);

                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseInfo.Users.ID,user.getId());
                contentValues.put(DatabaseInfo.Users.NAME,user.getName());
                contentValues.put(DatabaseInfo.Users.USER_NAME,user.getUsername());
                contentValues.put(DatabaseInfo.Users.EMAIL,user.getEmail());
                contentValues.put(DatabaseInfo.Users.WEBSITE,user.getWebsite());
                contentValues.put(DatabaseInfo.Users.PHONE,user.getPhone());

                contentValues.put(DatabaseInfo.Users.STREET,user.getStreet());
                contentValues.put(DatabaseInfo.Users.SUITE,user.getSuite());
                contentValues.put(DatabaseInfo.Users.ZIPCODE,user.getZipcode());
                contentValues.put(DatabaseInfo.Users.CITY,user.getCity());

                contentValues.put(DatabaseInfo.Users.LATITUDE,user.getLatitude());
                contentValues.put(DatabaseInfo.Users.LONGITUDE,user.getLongitude());

                contentValues.put(DatabaseInfo.Users.COMPANY_NAME,user.getCompany_name());
                contentValues.put(DatabaseInfo.Users.CATCH_PHRASE,user.getCatch_phrase());
                contentValues.put(DatabaseInfo.Users.BS,user.getBs());
                contentValues.put(DatabaseInfo.Users.IS_SELF,0);

                database.insert(DatabaseInfo.Users.TABLE_NAME,null,contentValues);

            }

            database.close();

        }catch (Exception e){
            e.printStackTrace();

            return false;
        }


        return true;
    }


    public List<UserModel> getUsersWhoAreNotFriends(){

        List<UserModel> users = new ArrayList<>();

        String query = "SELECT * FROM "+ DatabaseInfo.Users.TABLE_NAME + " WHERE "+ DatabaseInfo.Users.IS_SELF + " == 0";

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query,null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){

            UserModel user = new UserModel();
            user.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Users.ID)));
            user.setName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.NAME)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.USER_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.EMAIL)));
            user.setCompany_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.COMPANY_NAME)));

            if (!isFriend(user.getId()))
                users.add(user);

        }

        cursor.close();
        database.close();

        return users;
    }

    public List<UserModel> getUsers(){

        List<UserModel> users = new ArrayList<>();

        String query = "SELECT * FROM "+ DatabaseInfo.Users.TABLE_NAME + " WHERE "+ DatabaseInfo.Users.IS_SELF + " == 0";

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query,null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){

            UserModel user = new UserModel();
            user.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Users.ID)));
            user.setName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.NAME)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.USER_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.EMAIL)));
            user.setWebsite(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.WEBSITE)));
            user.setPhone(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.PHONE)));

            user.setSuite(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.SUITE)));
            user.setStreet(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.STREET)));
            user.setCity(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.CITY)));
            user.setZipcode(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.ZIPCODE)));

            user.setLatitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.LATITUDE)));
            user.setLongitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.LONGITUDE)));

            user.setCompany_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.COMPANY_NAME)));
            user.setCatch_phrase(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.CATCH_PHRASE)));
            user.setBs(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.BS)));

            users.add(user);

        }

        database.close();
        cursor.close();

        return users;
    }

    public UserModel getUser(int id){

        UserModel user = new UserModel();
        SQLiteDatabase database = this.getWritableDatabase();

        String query = "SELECT * FROM "+ DatabaseInfo.Users.TABLE_NAME+" WHERE "+ DatabaseInfo.Users.ID + " == "+id;

        Cursor cursor = database.rawQuery(query,null);

        if (cursor!= null){

            cursor.moveToFirst();

            user.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Users.ID)));
            user.setName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.NAME)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.USER_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.EMAIL)));
            user.setWebsite(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.WEBSITE)));
            user.setPhone(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.PHONE)));

            user.setSuite(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.SUITE)));
            user.setStreet(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.STREET)));
            user.setCity(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.CITY)));
            user.setZipcode(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.ZIPCODE)));

            user.setLatitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.LATITUDE)));
            user.setLongitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.LONGITUDE)));

            user.setCompany_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.COMPANY_NAME)));
            user.setCatch_phrase(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.CATCH_PHRASE)));
            user.setBs(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.BS)));

            cursor.close();
        }

        database.close();


        return user;

    }

    public Boolean saveUserDetails(UserModel user){

        try {

            SQLiteDatabase database = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseInfo.Users.ID,-1);
            contentValues.put(DatabaseInfo.Users.NAME,user.getName());
            contentValues.put(DatabaseInfo.Users.USER_NAME,user.getUsername());
            contentValues.put(DatabaseInfo.Users.EMAIL,user.getEmail());
            contentValues.put(DatabaseInfo.Users.WEBSITE,user.getWebsite());
            contentValues.put(DatabaseInfo.Users.PHONE,user.getPhone());

            contentValues.put(DatabaseInfo.Users.STREET,user.getStreet());
            contentValues.put(DatabaseInfo.Users.SUITE,user.getSuite());
            contentValues.put(DatabaseInfo.Users.ZIPCODE,user.getZipcode());
            contentValues.put(DatabaseInfo.Users.CITY,user.getCity());

            contentValues.put(DatabaseInfo.Users.LATITUDE,user.getLatitude());
            contentValues.put(DatabaseInfo.Users.LONGITUDE,user.getLongitude());

            contentValues.put(DatabaseInfo.Users.COMPANY_NAME,user.getCompany_name());
            contentValues.put(DatabaseInfo.Users.CATCH_PHRASE,user.getCatch_phrase());
            contentValues.put(DatabaseInfo.Users.BS,user.getBs());

            contentValues.put(DatabaseInfo.Users.IS_SELF,1);

            database.insert(DatabaseInfo.Users.TABLE_NAME,null,contentValues);


            database.close();

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return true;
    }

    public Boolean updateDetails(UserModel user){

        try{

            SQLiteDatabase database = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseInfo.Users.ID,-1);
            contentValues.put(DatabaseInfo.Users.NAME,user.getName());
            contentValues.put(DatabaseInfo.Users.USER_NAME,user.getUsername());
            contentValues.put(DatabaseInfo.Users.EMAIL,user.getEmail());
            contentValues.put(DatabaseInfo.Users.WEBSITE,user.getWebsite());
            contentValues.put(DatabaseInfo.Users.PHONE,user.getPhone());

            contentValues.put(DatabaseInfo.Users.STREET,user.getStreet());
            contentValues.put(DatabaseInfo.Users.SUITE,user.getSuite());
            contentValues.put(DatabaseInfo.Users.ZIPCODE,user.getZipcode());
            contentValues.put(DatabaseInfo.Users.CITY,user.getCity());

            contentValues.put(DatabaseInfo.Users.LATITUDE,user.getLatitude());
            contentValues.put(DatabaseInfo.Users.LONGITUDE,user.getLongitude());

            contentValues.put(DatabaseInfo.Users.COMPANY_NAME,user.getCompany_name());
            contentValues.put(DatabaseInfo.Users.CATCH_PHRASE,user.getCatch_phrase());
            contentValues.put(DatabaseInfo.Users.BS,user.getBs());

            contentValues.put(DatabaseInfo.Users.IS_SELF,1);

            String selection = DatabaseInfo.Users.ID + " = ? ";
            String[] selectionArgs = { "-1" };


            database.update(DatabaseInfo.Users.TABLE_NAME,contentValues,selection,selectionArgs);
            database.close();

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return true;
    }

    public Boolean addFriend(FriendModel friend){

        try{

            SQLiteDatabase database = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseInfo.Friends.USER_ID,friend.getUser_id());
            contentValues.put(DatabaseInfo.Friends.LATITUDE,friend.getLatitude());
            contentValues.put(DatabaseInfo.Friends.LONGITUDE,friend.getLongitude());
            contentValues.put(DatabaseInfo.Friends.SAVED_AT,friend.getSaved_at());

            database.insert(DatabaseInfo.Friends.TABLE_NAME,null,contentValues);

            database.close();

            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name),Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(context.getString(R.string.are_friends_added),true);
            editor.apply();

        }catch (Exception e){
            e.printStackTrace();

            return false;
        }


        return true;

    }

    public Boolean isFriend(int user_id){

        SQLiteDatabase database = this.getWritableDatabase();

        String query = "SELECT * FROM "+ DatabaseInfo.Friends.TABLE_NAME+" WHERE "+ DatabaseInfo.Friends.USER_ID +" == "+user_id;

        Cursor cursor = database.rawQuery(query,null);

        if (cursor.getCount() <= 0){

            cursor.close();
            return false;
        }

        cursor.close();
        return true;
    }

    public FriendModel getFriendDetails(int user_id){

        FriendModel friendModel = new FriendModel();
        String query = "SELECT * FROM "+ DatabaseInfo.Friends.TABLE_NAME+" WHERE "+ DatabaseInfo.Friends.USER_ID+" == "+user_id;
        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(query,null);

        if (cursor.moveToFirst()){

            friendModel.setLongitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.LONGITUDE)));
            friendModel.setLatitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.LATITUDE)));
            friendModel.setSaved_at(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.SAVED_AT)));
            friendModel.setUser_id(user_id);

            cursor.close();
        }

        database.close();

        return friendModel;
    }

    public Boolean deleteFriend(int user_id){

        try{

            SQLiteDatabase database = this.getWritableDatabase();

            String selection = DatabaseInfo.Friends.USER_ID + " == ";
            String[] selectionArgs = { ""+user_id };

            /*String query = "DELETE FROM "+ DatabaseInfo.Friends.TABLE_NAME + " WHERE "+ DatabaseInfo.Friends.USER_ID + " == "+user_id;
            database.execSQL(query);*/

            database.delete(DatabaseInfo.Friends.TABLE_NAME,selection,selectionArgs);

            database.close();


        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /*public ArrayList<FriendModel> getFriends(){

        ArrayList<FriendModel> friends = new ArrayList<>();

        try{

            SQLiteDatabase database = this.getWritableDatabase();

            String query = "SELECT * FROM "+ DatabaseInfo.Friends.TABLE_NAME;

            Cursor cursor = database.rawQuery(query,null);

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){

                FriendModel friend = new FriendModel();
                friend.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Friends.ID)));
                friend.setUser_id(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Friends.USER_ID)));
                friend.setLatitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.LATITUDE)));
                friend.setLongitude(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.LONGITUDE)));
                friend.setSaved_at(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Friends.SAVED_AT)));

                final UserModel userDetails = getUser(friend.getUser_id());
                friend.setName(userDetails.getName());
                friend.setUser_name(userDetails.getUsername());

                Log.d("Friend","----");
                Log.d("Name",friend.getName());
                Log.d("User_id",""+friend.getUser_id());

                friends.add(friend);
            }

            cursor.close();

            database.close();

        }catch (Exception e){
            e.printStackTrace();
        }


        return friends;
    }*/

    public ArrayList<FriendModel> getFriendsTrial() {

        ArrayList<FriendModel> friends = new ArrayList<>();

        try {

            SQLiteDatabase database = this.getWritableDatabase();

            String query = "SELECT " + DatabaseInfo.Friends.TABLE_NAME+"."+DatabaseInfo.Friends.ID+","
                    + DatabaseInfo.Friends.USER_ID+","
                    + DatabaseInfo.Users.NAME+","
                    + DatabaseInfo.Users.USER_NAME +" FROM " + DatabaseInfo.Friends.TABLE_NAME + ","+ DatabaseInfo.Users.TABLE_NAME
                    +" WHERE "+ DatabaseInfo.Friends.USER_ID+" == "+ DatabaseInfo.Users.TABLE_NAME+"."+DatabaseInfo.Users.ID;

            Log.d("Get friends query",query);

            Cursor cursor = database.rawQuery(query, null);

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                FriendModel friend = new FriendModel();
                friend.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Friends.ID)));
                friend.setUser_id(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Friends.USER_ID)));
                friend.setName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.NAME)));
                friend.setUser_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Users.USER_NAME)));
                friends.add(friend);

                Log.d("Friend","----");
                Log.d("Name",friend.getName());
                Log.d("User_id",""+friend.getUser_id());
                Log.d("Id",""+friend.getId());
            }

            cursor.close();
            database.close();

        }catch (Exception e){
            e.printStackTrace();
        }

        return friends;

    }

}
