package com.pranitkulkarni.bacchallenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pranitkulkarni on 8/3/17.
 */

public class AdapterFriendsChips extends RecyclerView.Adapter<AdapterFriendsChips.myViewHolder>{

    private ArrayList<FriendModel> friends = new ArrayList<>();
    private Context context;


    public AdapterFriendsChips(Context context,ArrayList<FriendModel> friends){
        this.friends = friends;
        this.context = context;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new myViewHolder(LayoutInflater.from(context).inflate(R.layout.chip_friend,parent,false));
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {

        final FriendModel friend = friends.get(position);
        holder.nameTv.setText(friend.getName());
        holder.usernameTv.setText(friend.getUser_name());
        holder.initialTv.setText(friend.getName().substring(0,1));

        holder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,UserDetails.class);
                intent.putExtra("user_id",friend.getUser_id());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public static class myViewHolder extends RecyclerView.ViewHolder{

        TextView nameTv,usernameTv,initialTv;
        View clickLayout;

        public myViewHolder(View itemView) {
            super(itemView);

            clickLayout = itemView.findViewById(R.id.clickLayout);
            nameTv = (TextView) itemView.findViewById(R.id.name);
            usernameTv = (TextView) itemView.findViewById(R.id.user_name);
            initialTv = (TextView) itemView.findViewById(R.id.initial);

        }

    }
}
