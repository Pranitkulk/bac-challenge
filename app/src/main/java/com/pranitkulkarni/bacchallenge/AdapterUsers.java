package com.pranitkulkarni.bacchallenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pranitkulkarni on 8/1/17.
 */

public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.myViewHolder> {

    private List<UserModel> usersList = new ArrayList<>();
    private Context context;

    public AdapterUsers(Context context,List<UserModel> usersList){
        this.usersList = usersList;
        this.context = context;

    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new myViewHolder(LayoutInflater.from(context).inflate(R.layout.users_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {

        final UserModel userModel = usersList.get(position);

        holder.nameTv.setText(userModel.getName());
        holder.usernameTv.setText(userModel.getUsername());
        holder.emailTv.setText(userModel.getEmail());
        holder.companyNameTv.setText(userModel.getCompany_name());

        holder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,UserDetails.class);
                intent.putExtra("user_id",userModel.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public static class myViewHolder extends RecyclerView.ViewHolder{

        TextView nameTv,usernameTv,emailTv,companyNameTv;
        View clickLayout;

        public myViewHolder(View itemView){
            super(itemView);

            clickLayout = itemView.findViewById(R.id.clickLayout);
            nameTv = (TextView) itemView.findViewById(R.id.name);
            usernameTv = (TextView) itemView.findViewById(R.id.user_name);
            emailTv = (TextView) itemView.findViewById(R.id.email);
            companyNameTv = (TextView) itemView.findViewById(R.id.company_name);

        }

    }

}
