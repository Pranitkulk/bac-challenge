package com.pranitkulkarni.bacchallenge;

import android.*;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

import java.util.Calendar;

public class UserDetails extends AppCompatActivity {

    TextView nameTv,usernameTv,phoneTv,websiteTv,emailTv,companyNameTv,addressTv,companyExtrasTv;
    int id = 0;
    UserModel user;
    private FusedLocationProviderClient locationProviderClient;
    FloatingActionButton addFriendBtn;
    String latitude = "",longitude="";
    CoordinatorLayout coordinatorLayout;
    DatabaseManager databaseManager;
    View locationCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nameTv = (TextView)findViewById(R.id.name);
        usernameTv = (TextView)findViewById(R.id.user_name);
        emailTv = (TextView)findViewById(R.id.email);
        phoneTv = (TextView)findViewById(R.id.phone);
        websiteTv = (TextView)findViewById(R.id.website);
        companyNameTv = (TextView)findViewById(R.id.company_name);
        addressTv = (TextView)findViewById(R.id.address);
        companyExtrasTv = (TextView)findViewById(R.id.company_extras);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);
        addFriendBtn = (FloatingActionButton)findViewById(R.id.add_friend);
        locationCard = findViewById(R.id.location_card);

        databaseManager = new DatabaseManager(UserDetails.this);

        id = getIntent().getIntExtra("user_id",0);

        if (id == -1) {   // YOU
            setTitle("YOU");
            addFriendBtn.setVisibility(View.GONE);

            FloatingActionButton editFab = (FloatingActionButton)findViewById(R.id.edit);
            editFab.setVisibility(View.VISIBLE);
            editFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(UserDetails.this,AddUserDetails.class);
                    intent.putExtra("is_editing",true);
                    startActivityForResult(intent,1);

                }
            });
        }
        else
            setTitle("User details");

        user = databaseManager.getUser(id);


        if (databaseManager.isFriend(id) || id == -1) {
            addFriendBtn.setVisibility(View.GONE);

            if (id != -1){  // Friend's page

                locationCard.setVisibility(View.VISIBLE);
                TextView locationNote = (TextView)findViewById(R.id.location_note);
                TextView locationCoordinates = (TextView)findViewById(R.id.location_coordinates);

                locationNote.setText("Your location when you added "+user.getName()+" as your friend");

                final FriendModel friendModel = databaseManager.getFriendDetails(id);

                String coordinates = "Lat: "+friendModel.getLatitude() + "\nLng: "+friendModel.getLongitude();
                locationCoordinates.setText(coordinates);

                findViewById(R.id.checkOnMaps).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String coordinates = "geo:"+friendModel.getLatitude()+","+friendModel.getLongitude();
                        Uri gmmIntentUri = Uri.parse(coordinates);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }

                    }
                });
            }

        }
        else {
            locationProviderClient = LocationServices.getFusedLocationProviderClient(UserDetails.this);

            if (ActivityCompat.checkSelfPermission(UserDetails.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserDetails.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(UserDetails.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
            else
                getLocation();
        }


        nameTv.setText(user.getName());
        usernameTv.setText(user.getUsername());
        emailTv.setText(user.getEmail());
        phoneTv.setText(user.getPhone());
        websiteTv.setText(user.getWebsite());
        companyNameTv.setText(user.getCompany_name());

        String address = user.getSuite() + "\n" + user.getStreet() + "\n" + user.getCity() + ", " + user.getZipcode();
        addressTv.setText(address);

        String extras = "Catchphrase: "+ user.getCatch_phrase() + "\nBS: "+user.getBs();
        companyExtrasTv.setText(extras);


        addFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addFriend();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        else if (item.getItemId() == R.id.unfriend){

            if (databaseManager.deleteFriend(id)){
                locationCard.setVisibility(View.GONE);

                addFriendBtn.setVisibility(View.VISIBLE);

                locationProviderClient = LocationServices.getFusedLocationProviderClient(UserDetails.this);

                if (ActivityCompat.checkSelfPermission(UserDetails.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserDetails.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(UserDetails.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
                }
                else
                    getLocation();
            }
            else
                Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();

        if (databaseManager.isFriend(id))
            menu.add(0,R.id.unfriend,Menu.NONE,"Unfriend");

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK){

                final UserModel userModel = databaseManager.getUser(data.getIntExtra("id",-1));
                nameTv.setText(userModel.getName());
                usernameTv.setText(userModel.getUsername());
                emailTv.setText(userModel.getEmail());
                phoneTv.setText(userModel.getPhone());
                websiteTv.setText(userModel.getWebsite());
                companyNameTv.setText(userModel.getCompany_name());

                String address = userModel.getSuite() + "\n" + userModel.getStreet() + "\n" + userModel.getCity() + ", " + userModel.getZipcode();
                addressTv.setText(address);

                String extras = "Catchphrase: "+ userModel.getCatch_phrase() + "\nBS: "+userModel.getBs();
                companyExtrasTv.setText(extras);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("name",userModel.getName());
                returnIntent.putExtra("username",userModel.getUsername());
                setResult(Activity.RESULT_OK,returnIntent);

            }

        }

    }

    private void getLocation(){

        try{

            locationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location!=null){
                        Log.d("Location obtained","");

                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());

                        Log.d("Latitude",latitude);
                        Log.d("Longitude",longitude);
                    }
                    else {
                        Log.d("Location is null", "");
                    }

                }
            });


        }catch (SecurityException e){
            e.printStackTrace();
        }



    }

    private void addFriend(){

            if (latitude.isEmpty() || longitude.isEmpty()){

                ActivityCompat.requestPermissions(UserDetails.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
            else {

                FriendModel friend = new FriendModel();
                friend.setUser_id(id);
                friend.setLatitude(latitude);
                friend.setLongitude(longitude);
                friend.setSaved_at(String.valueOf(Calendar.getInstance().getTime()));

                if(databaseManager.addFriend(friend)) {
                    addFriendBtn.setVisibility(View.GONE);
                    Snackbar.make(coordinatorLayout, user.getName() + " has been added as your friend", Snackbar.LENGTH_LONG).show();
                }
                else
                    Snackbar.make(coordinatorLayout,"Something went wrong. Try again",Snackbar.LENGTH_LONG).show();
            }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLocation();
            // TODO Do something if permission is not given...

        }
    }

}
