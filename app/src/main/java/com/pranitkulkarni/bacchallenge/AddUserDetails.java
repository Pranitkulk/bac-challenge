package com.pranitkulkarni.bacchallenge;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

import java.util.ArrayList;

public class AddUserDetails extends AppCompatActivity {

    EditText nameEt, usernameEt, emailEt, phoneEt, websiteEt, suiteEt, streetEt, cityEt, zipcodeEt, companyNameEt, catchPhraseEt, bsEt;
    final String RED_COLOR = "#F44336";
    CoordinatorLayout coordinatorLayout;
    private FusedLocationProviderClient locationProviderClient;
    String latitude="",longitude="";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Boolean isEditing;
    DatabaseManager databaseManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Add Details");

        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        editor = sharedPreferences.edit();

        databaseManager = new DatabaseManager(AddUserDetails.this);

        isEditing = getIntent().getBooleanExtra("is_editing",false);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(AddUserDetails.this);

        if (ActivityCompat.checkSelfPermission(AddUserDetails.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddUserDetails.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(AddUserDetails.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        else {

            locationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location!=null){
                        Log.d("Location obtained","");

                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());

                        Log.d("Latitude",latitude);
                        Log.d("Longitude",longitude);
                    }
                    else
                        Log.d("Location is null","");

                }
            });
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        nameEt = (EditText) findViewById(R.id.name);
        usernameEt = (EditText) findViewById(R.id.user_name);
        emailEt = (EditText) findViewById(R.id.email);
        websiteEt = (EditText) findViewById(R.id.website);
        phoneEt = (EditText) findViewById(R.id.phone);

        suiteEt = (EditText) findViewById(R.id.suite);
        streetEt = (EditText) findViewById(R.id.street);
        cityEt = (EditText) findViewById(R.id.city);
        zipcodeEt = (EditText) findViewById(R.id.zipcode);

        companyNameEt = (EditText) findViewById(R.id.company_name);
        catchPhraseEt = (EditText) findViewById(R.id.catch_phrase);
        bsEt = (EditText) findViewById(R.id.bs);

        if (isEditing){

            setTitle("Edit details");

            final UserModel user = databaseManager.getUser(-1);
            nameEt.setText(user.getName());
            usernameEt.setText(user.getUsername());
            emailEt.setText(user.getEmail());
            phoneEt.setText(user.getPhone());
            websiteEt.setText(user.getWebsite());
            suiteEt.setText(user.getSuite());
            streetEt.setText(user.getStreet());
            cityEt.setText(user.getCity());
            zipcodeEt.setText(user.getZipcode());
            companyNameEt.setText(user.getCompany_name());
            catchPhraseEt.setText(user.getCatch_phrase());
            bsEt.setText(user.getBs());

        }


        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ArrayList<EditText> fields = new ArrayList<>();

                fields.add(nameEt);
                fields.add(usernameEt);
                fields.add(emailEt);
                fields.add(phoneEt);
                fields.add(websiteEt);

                fields.add(suiteEt);
                fields.add(streetEt);
                fields.add(cityEt);
                fields.add(zipcodeEt);

                fields.add(companyNameEt);
                fields.add(catchPhraseEt);
                fields.add(bsEt);

                if (checkFields(fields)) {

                    UserModel user = new UserModel();
                    user.setName(nameEt.getText().toString());
                    user.setUsername(usernameEt.getText().toString());
                    user.setEmail(emailEt.getText().toString());
                    user.setPhone(phoneEt.getText().toString());
                    user.setWebsite(websiteEt.getText().toString());

                    user.setSuite(suiteEt.getText().toString());
                    user.setStreet(streetEt.getText().toString());
                    user.setCity(cityEt.getText().toString());
                    user.setZipcode(zipcodeEt.getText().toString());

                    user.setCompany_name(companyNameEt.getText().toString());
                    user.setCatch_phrase(catchPhraseEt.getText().toString());
                    user.setBs(bsEt.getText().toString());

                    user.setLongitude(longitude);
                    user.setLatitude(latitude);

                    if (isEditing) {

                        if (databaseManager.updateDetails(user)){
                            Intent intent = new Intent();
                            intent.putExtra("id",-1);
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                        else
                            Snackbar.make(coordinatorLayout,"Something went wrong! Please try again.",Snackbar.LENGTH_LONG).show();


                    }
                    else{   // Adding for the 1st time...

                        if (databaseManager.saveUserDetails(user)){
                            editor.putBoolean(getString(R.string.are_user_details_saved),true);
                            editor.apply();
                            setResult(RESULT_OK);
                            finish();
                        }
                        else
                            Snackbar.make(coordinatorLayout,"Something went wrong! Please try again.",Snackbar.LENGTH_LONG).show();
                    }


                }
                else {
                    Snackbar.make(coordinatorLayout,"Enter required information to continue",Snackbar.LENGTH_LONG).show();
                }

            }
        });
    }

    private Boolean checkFields(ArrayList<EditText> fields){

        Boolean flag = true;

        for(int i=0; i< fields.size(); i++){
            EditText editText = fields.get(i);

            if (editText.getText().toString().trim().length() <= 0) {
                editText.setHintTextColor(Color.parseColor(RED_COLOR));
                flag = false;
            }


        }

        return flag;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                try{

                    locationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {

                            if (location!=null){
                                Log.d("Location obtained","");

                                latitude = String.valueOf(location.getLatitude());
                                longitude = String.valueOf(location.getLongitude());

                                Log.d("Latitude",latitude);
                                Log.d("Longitude",longitude);
                            }
                            else
                                Log.d("Location is null","");

                        }
                    });


                }catch (SecurityException e){
                    e.printStackTrace();
                }




            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }


}
