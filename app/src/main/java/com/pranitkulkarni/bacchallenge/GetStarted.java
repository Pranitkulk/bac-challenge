package com.pranitkulkarni.bacchallenge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetStarted extends AppCompatActivity {

    private List<UserModel> usersList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    AppCompatButton getStartedBtn;
    TextView helpText;
    ProgressBar progressBar;
    AppCompatButton tryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        helpText = (TextView)findViewById(R.id.helpText);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        tryAgain = (AppCompatButton)findViewById(R.id.try_again);
        editor = sharedPreferences.edit();


        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadUsers();
            }
        });

        loadUsers();

        getStartedBtn = (AppCompatButton)findViewById(R.id.get_started);

        getStartedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                startActivity(new Intent(GetStarted.this,MainActivity.class));

            }
        });
    }

    private void loadUsers(){

        if (new CheckNetworkConnection(GetStarted.this).haveNetworkConnection()) {
            tryAgain.setVisibility(View.GONE);
            new GetResponse().execute(getString(R.string.users_url));
        }
        else {
            helpText.setText("NO INTERNET CONNECTION\n\n Connect to internet to load users");
            tryAgain.setVisibility(View.VISIBLE);
        }

    }

    private class GetResponse extends AsyncTask<String , Void ,String> {
        String server_response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {

            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    server_response = readStream(urlConnection.getInputStream());
                    Log.v("CatalogClient", server_response);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Response", "" + server_response);

            try{

                JSONArray jsonArray = new JSONArray(server_response);

                Log.d("Array",jsonArray.toString());

                if (jsonArray.length() >= 0){

                    for (int i=0;i<jsonArray.length();i++){

                        final JSONObject rowObj = jsonArray.getJSONObject(i);

                        UserModel userModel = new UserModel();
                        userModel.setId(rowObj.getInt("id"));
                        userModel.setName(rowObj.getString("name"));
                        userModel.setUsername(rowObj.getString("username"));
                        userModel.setEmail(rowObj.getString("email"));
                        userModel.setPhone(rowObj.getString("phone"));
                        userModel.setWebsite(rowObj.getString("website"));

                        JSONObject companyObj = rowObj.getJSONObject("company");
                        userModel.setCompany_name(companyObj.getString("name"));
                        userModel.setCatch_phrase(companyObj.getString("catchPhrase"));
                        userModel.setBs(companyObj.getString("bs"));

                        JSONObject addressObj = rowObj.getJSONObject("address");
                        userModel.setSuite(addressObj.getString("suite"));
                        userModel.setStreet(addressObj.getString("street"));
                        userModel.setCity(addressObj.getString("city"));
                        userModel.setZipcode(addressObj.getString("zipcode"));

                        JSONObject geoObj = addressObj.getJSONObject("geo");
                        userModel.setLatitude(geoObj.getString("lat"));
                        userModel.setLongitude(geoObj.getString("lng"));

                        usersList.add(userModel);

                    }

                    progressBar.setVisibility(View.GONE);

                    if(new DatabaseManager(GetStarted.this).saveUsers(usersList)) {
                        editor.putBoolean(getString(R.string.are_users_saved), true);
                        editor.commit();


                        helpText.setText("You are all set!");
                        getStartedBtn.setVisibility(View.VISIBLE);
                    }
                    else
                        somethingWentWrong();

                }

            }catch (JSONException j){
                j.printStackTrace();
                somethingWentWrong();
            }




        }
    }

    private void somethingWentWrong(){
        Log.d("Something went wrong","Downloading users");
        helpText.setText("Something went wrong!\nPlease try again");
        tryAgain.setVisibility(View.VISIBLE);
    }

    // convert response to a String
    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }
}
