package com.pranitkulkarni.bacchallenge;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentYou extends Fragment {

    SharedPreferences sharedPreferences;
    View profileLayout,noDetailsLayout;
    TextView nameTv,usernameTv;
    AppCompatButton detailsBtn,addNowBtn,addFriendBtn;
    RecyclerView recyclerView;
    View friendsList,noFriendsLayout;


    public FragmentYou() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_you, container, false);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        profileLayout = view.findViewById(R.id.profile_layout);
        noDetailsLayout = view.findViewById(R.id.no_details_layout);

        detailsBtn = (AppCompatButton)view.findViewById(R.id.details);
        addNowBtn = (AppCompatButton)view.findViewById(R.id.add_now);
        addFriendBtn= (AppCompatButton)view.findViewById(R.id.add_friend);

        friendsList = view.findViewById(R.id.friends_layout);
        noFriendsLayout = view.findViewById(R.id.no_friends_card);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        nameTv = (TextView)view.findViewById(R.id.name);
        usernameTv = (TextView)view.findViewById(R.id.user_name);

        if (sharedPreferences.getBoolean(getString(R.string.are_user_details_saved),false))
            setLayout();
        else {

            noDetailsLayout.setVisibility(View.VISIBLE);
            profileLayout.setVisibility(View.GONE);

            view.findViewById(R.id.add_user_details).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityForResult(new Intent(getActivity(),AddUserDetails.class),1);

                }
            });
        }

        return view;
    }

    private void setLayout(){

        profileLayout.setVisibility(View.VISIBLE);
        noDetailsLayout.setVisibility(View.GONE);



        detailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),UserDetails.class);
                intent.putExtra("user_id",-1);
                startActivityForResult(intent,3);

            }
        });

        UserModel userDetails = new DatabaseManager(getActivity()).getUser(-1);

        nameTv.setText(userDetails.getName());
        usernameTv.setText(userDetails.getUsername());

        Log.d("Name",userDetails.getName());
        Log.d("Username",userDetails.getUsername());
        Log.d("Email",userDetails.getEmail());
        Log.d("Phone",userDetails.getPhone());
        Log.d("Website",userDetails.getWebsite());

        Log.d("Suite",userDetails.getSuite());
        Log.d("Street",userDetails.getStreet());
        Log.d("City",userDetails.getCity());
        Log.d("Zipcode",userDetails.getZipcode());

        Log.d("Company name",userDetails.getCompany_name());
        Log.d("CP",userDetails.getCatch_phrase());
        Log.d("BS",userDetails.getBs());

        Log.d("latitude",userDetails.getLatitude());
        Log.d("longitude",userDetails.getLongitude());

        if (sharedPreferences.getBoolean(getString(R.string.are_friends_added),false)){

            noFriendsLayout.setVisibility(View.GONE);
            friendsList.setVisibility(View.VISIBLE);

            // add button action

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
            recyclerView.setAdapter(new AdapterFriendsChips(getActivity(),new DatabaseManager(getActivity()).getFriendsTrial()));

            addFriendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityForResult(new Intent(getActivity(),AddFriend.class),2);

                }
            });

        }
        else {

            addNowBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(getActivity(),AddFriend.class),2);
                }
            });

        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1){

            if (resultCode == Activity.RESULT_OK){  // User details added..

                setLayout();

            }

        }

        if (requestCode == 2){  // Friend added..

            if (resultCode == Activity.RESULT_OK){

                noFriendsLayout.setVisibility(View.GONE);
                friendsList.setVisibility(View.VISIBLE);

                // add button action

                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
                recyclerView.setAdapter(new AdapterFriendsChips(getActivity(),new DatabaseManager(getActivity()).getFriendsTrial()));

                addFriendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        startActivityForResult(new Intent(getActivity(),AddFriend.class),2);

                    }
                });
            }

        }

        if (requestCode == 3){

            if (resultCode == Activity.RESULT_OK){

                nameTv.setText(data.getStringExtra("name"));
                usernameTv.setText(data.getStringExtra("username"));

            }

        }
    }
}
