package com.pranitkulkarni.bacchallenge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_users:
                    getSupportFragmentManager().beginTransaction().replace(R.id.content,new FragmentUsersList()).commit();
                    return true;

                case R.id.navigation_you:
                    getSupportFragmentManager().beginTransaction().replace(R.id.content,new FragmentYou()).commit();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.app_name));

        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);

        if (sharedPreferences.getBoolean(getString(R.string.are_users_saved),false))
            getSupportFragmentManager().beginTransaction().add(R.id.content,new FragmentUsersList()).commit();
        else {
            startActivity(new Intent(this,GetStarted.class));   // Download users data here...
            finish();
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
