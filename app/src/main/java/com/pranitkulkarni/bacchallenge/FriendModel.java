package com.pranitkulkarni.bacchallenge;

/**
 * Created by pranitkulkarni on 8/2/17.
 */

public class FriendModel {

    private String latitude = "",longitude = "",saved_at="",name="",user_name="";
    private int id = 0, user_id=0;

    public void setId(int id) {
        this.id = id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setSaved_at(String saved_at) {
        this.saved_at = saved_at;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getSaved_at() {
        return saved_at;
    }

    public String getName() {
        return name;
    }

    public String getUser_name() {
        return user_name;
    }
}
