package com.pranitkulkarni.bacchallenge;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.pranitkulkarni.bacchallenge.Database.DatabaseManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddFriend extends AppCompatActivity {

    RecyclerView recyclerView;
    List<UserModel> users = new ArrayList<>();
    DatabaseManager databaseManager;
    String latitude = "",longitude="";
    CoordinatorLayout coordinatorLayout;
    private FusedLocationProviderClient locationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Add Friend");

        databaseManager = new DatabaseManager(AddFriend.this);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(AddFriend.this);

        if (ActivityCompat.checkSelfPermission(AddFriend.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddFriend.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(AddFriend.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        else
            getLocation();

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        users = databaseManager.getUsersWhoAreNotFriends();

        if (users.size() == 0){
            Toast.makeText(AddFriend.this,"You have added everyone as your friend.",Toast.LENGTH_LONG).show();
            finish();
        }
        else {
            recyclerView.setLayoutManager(new LinearLayoutManager(AddFriend.this));
            recyclerView.setAdapter(new AdapterSelectFriends());
        }


    }

    private void getLocation(){

        try{

            locationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location!=null){
                        Log.d("Location obtained","");

                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());

                        Log.d("Latitude",latitude);
                        Log.d("Longitude",longitude);
                    }
                    else {
                        Log.d("Location is null", "");
                    }

                }
            });


        }catch (SecurityException e){
            e.printStackTrace();
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLocation();
            else {
                Snackbar.make(coordinatorLayout, "Can't add your location unless you give the permission.", Snackbar.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(AddFriend.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
            }

        }
    }

    private class AdapterSelectFriends extends RecyclerView.Adapter<AdapterSelectFriends.myViewHolder> {


        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new myViewHolder(LayoutInflater.from(AddFriend.this).inflate(R.layout.users_list_item,parent,false));
        }

        @Override
        public void onBindViewHolder(myViewHolder holder, int position) {

            final UserModel userModel = users.get(position);

            holder.nameTv.setText(userModel.getName());
            holder.usernameTv.setText(userModel.getUsername());
            holder.emailTv.setText(userModel.getEmail());
            holder.companyNameTv.setText(userModel.getCompany_name());

            holder.clickLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FriendModel friend = new FriendModel();
                    friend.setUser_id(userModel.getId());
                    friend.setName(userModel.getName());
                    friend.setUser_name(userModel.getUsername());
                    friend.setSaved_at(String.valueOf(Calendar.getInstance().getTime()));
                    friend.setLatitude(latitude);
                    friend.setLongitude(longitude);

                    if (databaseManager.addFriend(friend)){
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                    else
                        Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();

                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        class myViewHolder extends RecyclerView.ViewHolder{

            TextView nameTv,usernameTv,emailTv,companyNameTv;
            View clickLayout;

            public myViewHolder(View itemView){
                super(itemView);

                clickLayout = itemView.findViewById(R.id.clickLayout);
                nameTv = (TextView) itemView.findViewById(R.id.name);
                usernameTv = (TextView) itemView.findViewById(R.id.user_name);
                emailTv = (TextView) itemView.findViewById(R.id.email);
                companyNameTv = (TextView) itemView.findViewById(R.id.company_name);

            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }

}
